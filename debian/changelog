python-hgapi (1.7.3+git20170127.dd8fb7b-5) unstable; urgency=medium

  * Team Upload
  * Remove reference to obsolete python_distutils
  * Use dh-sequence-python3

  [ Debian Janitor ]
  * Update standards version to 4.6.2, no changes needed.

  [ Nick Morrott ]
  * Orphan Package

 -- Alexandre Detiste <tchet@debian.org>  Sun, 23 Feb 2025 17:22:29 +0100

python-hgapi (1.7.3+git20170127.dd8fb7b-4) unstable; urgency=medium

  * d/control:
    - Update standards version to 4.6.0, no changes needed.
  * d/copyright:
    - Refresh years of Debian copyright
  * d/t/control:
    - Add python3-all to test dependencies
  * d/watch:
    - Update pattern for GitHub archive URLs
  * d/copyright: Use 4-digit years in Debian copyright ranges

 -- Nick Morrott <nickm@debian.org>  Sun, 06 Feb 2022 05:24:05 +0000

python-hgapi (1.7.3+git20170127.dd8fb7b-3) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control:
    - Update Maintainer field with new Debian Python Team contact address.
    - Update Vcs-* fields with new Debian Python Team Salsa layout.

  [ Nick Morrott ]
  * d/control:
    - Declare compliance with Debian Policy 4.5.1
  * d/copyright:
    - Refresh Debian copyright years
  * d/salsa-ci.yml:
    - Add Salsa CI pipeline
  * d/s/lintian-overrides:
    - Add override
  * d/u/metadata:
    - Remove obsolete fields Contact, Name (already present in
      machine-readable debian/copyright).

 -- Nick Morrott <nickm@debian.org>  Sun, 07 Feb 2021 10:28:53 +0000

python-hgapi (1.7.3+git20170127.dd8fb7b-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.

  [ Nick Morrott ]
  * d/control:
    - Declare compliance with Debian Policy 4.5.0
    - Bump debhelper compatibility level to 13
    - Add Rules-Requires-Root field
    - Update email address in Uploaders
  * d/copyright:
    - Bump years of Debian copyright
  * d/gbp.conf:
    - Use pristine-tar
  * d/tests:
    - Run autopkgtests against supported Python versions

 -- Nick Morrott <nickm@debian.org>  Fri, 31 Jul 2020 15:18:05 +0100

python-hgapi (1.7.3+git20170127.dd8fb7b-1) unstable; urgency=medium

  * Initial release (Closes: #913976)

 -- Nick Morrott <knowledgejunkie@gmail.com>  Fri, 28 Dec 2018 22:42:08 +0000
